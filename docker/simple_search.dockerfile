FROM python:3.9

RUN mkdir /simple_search_app/
WORKDIR /simple_search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY data data
COPY src src
COPY src/search/simple_search/service.py src/search/service.py


CMD ["python", "src/search/service.py"]
