FROM python:3.9

RUN mkdir /user_app/
WORKDIR /user_app/

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY data data
COPY src src
COPY src/search/user/service.py src/search/service.py


CMD ["python", "src/search/service.py"]
