FROM python:3.9

RUN mkdir /search_in_shards_app/
WORKDIR /search_in_shards_app/

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY data data
COPY src src
COPY src/search/search_in_shards/service.py src/search/service.py


CMD ["python", "src/search/service.py"]
