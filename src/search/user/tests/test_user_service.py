import pytest
import time

from settings import USER_ADDR
import json
import requests


@pytest.fixture
def baseurl():
    return f'http://{USER_ADDR}'


@pytest.fixture
def ensure_running(baseurl):
    while True:
        time.sleep(30)
        try:
            response = requests.get(baseurl)
            assert response.status_code == 404
            break
        except requests.exceptions.ConnectionError:
            print('Waiting for search to run...')
            time.sleep(5)


def mocks():
    return {"gender": "female", "age": 23}, None


def service(user_id):
    return json.loads(requests.get(f'http://{USER_ADDR}/search?user_id={user_id}').content)


@pytest.mark.usefixtures("ensure_running")
def test_user_service(mocks):
    assert mocks == service(1), service(0)
