from flask import Flask, request
import json
from settings import USER_DATA_FILE
from common.data_source import CSV

app = Flask(__name__)
app.key = "user_id"
app.data_keys = ("gender", "age")


def init():
    data_source = CSV(USER_DATA_FILE)
    app.data = dict()
    data = data_source.read_data()
    for row in data:
        assert row[app.key] not in app.data, f'Key value {app.key}={row[app.key]} is not unique in self._data'
        app.data[row[app.key]] = {k: row[k] for k in app.data_keys}


def get_user_data(user_id):
    return app.data.get(user_id)


@app.route("/search", methods=["GET"])
def search():
    user_id = request.args.get("user_id")
    return json.dumps(get_user_data(int(user_id)))


if __name__ == "__main__":
    init()
    app.run(debug=True, host="0.0.0.0", port="5536")
