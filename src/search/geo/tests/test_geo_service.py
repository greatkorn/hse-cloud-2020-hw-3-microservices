import pytest
import time

from settings import GEO_ADDR
import json
import requests


@pytest.fixture
def baseurl():
    return f'http://{GEO_ADDR}'


@pytest.fixture
def ensure_running(baseurl):
    while True:
        time.sleep(30)
        try:
            response = requests.get(baseurl)
            assert response.status_code == 404
            break
        except requests.exceptions.ConnectionError:
            print('Waiting for search to run...')
            time.sleep(5)

def service(ip_addr):
    return json.loads(requests.get(f'http://{GEO_ADDR}/search?ip_addr={ip_addr}').content)


@pytest.fixture
def mocks():
    return service('2.115.14.192'), service('192.168.1.255'), service('abc')

@pytest.mark.usefixtures("ensure_running")
def test_geo_service(mocks):
    assert mocks == ({'region': 'Italy'}, None, None)
