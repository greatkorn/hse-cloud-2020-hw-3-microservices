import ipaddress as ip

import pandas as pd

from common.data_source import CSV
from flask import Flask, request
import json
from settings import GEO_DATA_FILE

app = Flask(__name__)


def init():
    data_source = CSV(GEO_DATA_FILE)
    app.data = data_source.read_data(to_dict=False)
    for col in ('network', 'country_name'):
        assert col in app.data.columns
    app.networks = app.data['network'].apply(ip.ip_network)


def get_geo_data(ip_addr):
    try:
        addr = ip.ip_address(ip_addr)
    except ValueError:
        return None
    addr_in_net = app.networks.apply(lambda x: addr in x)
    addr_in_net = addr_in_net[addr_in_net == True]
    if len(addr_in_net) > 0:
        return {'region': app.data.loc[addr_in_net.head(1).index].iloc[0].country_name}
    return None


@app.route("/search", methods=["GET"])
def search():
    ip_addr = request.args.get("ip_addr")
    return json.dumps(get_geo_data(ip_addr))


if __name__ == "__main__":
    init()
    app.run(debug=True, host="0.0.0.0", port="5536")
