import pytest
import time

import json
import requests
from settings import SEARCH_ADDR

@pytest.fixture
def baseurl():
    return f'http://{SEARCH_ADDR}'


@pytest.fixture
def ensure_running(baseurl):
    while True:
        time.sleep(30)
        try:
            response = requests.get(baseurl)
            assert response.status_code == 404
            break
        except requests.exceptions.ConnectionError:
            print('Waiting for search to run...')
            time.sleep(5)

def service(search_text, user_id, ip_addr):
    return json.loads(
        requests.get(f'http://{SEARCH_ADDR}/search?ip_addr={ip_addr}&user_id={user_id}&text={search_text}').content)


@pytest.fixture
def mock():
    return service(search_text='politician', user_id=25, ip_addr='3.103.8.10')

@pytest.mark.usefixtures("ensure_running")
def test_integration_works(mock):
    result_keys = [d['key'] for d in mock["search_results"]]
    expected_keys = [
        'Dutch crackdown risks hurting mainstream Muslims',
        'Dutch Filmmaker Murder Suspect Faces Terror Charges',
        'Surprise victory for Basescu in Romania',
        'BLAIR PEACE HOPES',
        'France Opens Judicial Inquiry Into Holocaust Doubter'
    ]
    assert sorted(result_keys) == sorted(expected_keys)
