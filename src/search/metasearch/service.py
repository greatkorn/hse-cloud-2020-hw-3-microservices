from flask import Flask, request
import requests
import json
import pandas as pd

from settings import USER_ADDR, GEO_ADDR, SEARCH_SHARDS_ADDR, DOCS_COLUMNS

app = Flask(__name__)


def get_user_data(user_id):
    return requests.get(f"http://{USER_ADDR}/search?user_id={user_id}").json()


def get_geo_data(ip):
    return requests.get(f"http://{GEO_ADDR}/search?ip_addr={ip}").json()


def get_search_data(user_data, geo_data, search_text):
    data = requests.get(
        f"http://{SEARCH_SHARDS_ADDR}/search?search_text={search_text}&user_data={user_data}&geo_data={geo_data}")
    resp = json.loads(data.content)
    df = pd.DataFrame.from_dict(resp)
    return df[DOCS_COLUMNS].to_dict("records")


def metasearch(search_text, user_id, ip):
    user_data = get_user_data(user_id)
    geo_data = get_geo_data(ip)
    return get_search_data(user_data, geo_data, search_text)


@app.route("/search", methods=["GET"])
def search():
    search_text = request.args.get("text")
    user_id = request.args.get("user_id")
    ip = request.args.get("ip_addr")
    return {"search_results": metasearch(search_text, user_id, ip)}


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port="5536")
