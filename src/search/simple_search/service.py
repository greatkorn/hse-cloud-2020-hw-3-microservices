from flask import Flask, request
import json
from common.data_source import CSV
import pandas as pd
import os
from settings import SEARCH_DOCUMENTS_DATA_FILES, DOCS_COLUMNS

app = Flask(__name__)


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


def _build_tokens_count(search_text):
    tokens = search_text.split()
    res = app.data['document'].apply(lambda x: stupid_count_tokens(tokens, x))
    res.name = None
    return res


def _get_geo_mask(geo_data=None):
    gd = geo_data.get('region') if geo_data is not None else None
    return app.data['region'] == gd


def _get_gender_mask(user_data=None):
    ud = user_data.get('gender', 'null') if user_data is not None else 'non-existing gender'
    return app.data['gender'].apply(lambda x: stupid_count_tokens([ud], x))


def _get_age_mask(user_data=None):
    user_age = int(user_data['age']) if user_data is not None else -1
    return app.data.apply(lambda x: x['age_from'] <= user_age <= x['age_to'], axis=1)


def _sort_by_rating_and_tokens(rating, tokens_count, key_md5):
    df = pd.concat([tokens_count, rating, key_md5], axis=1)
    return df.sort_values([0, 1, 'key_md5'], ascending=[False, False, False])


def get_search_data(search_text, user_data=None, geo_data=None, limit=10) -> pd.DataFrame:
    # this is some simple algorithm that came to my mind, does not need to be useful or good, just something working
    if search_text is None or search_text == '':
        return pd.DataFrame([], columns=DOCS_COLUMNS)
    tokens_count = _build_tokens_count(search_text)
    geo_mask = _get_geo_mask(geo_data)
    gender_mask = _get_gender_mask(user_data)
    age_mask = _get_age_mask(user_data)
    rating = geo_mask + gender_mask + age_mask
    df = _sort_by_rating_and_tokens(rating, tokens_count, app.data['key_md5'])
    return app.data.loc[df.head(limit).index]


@app.route("/search", methods=["GET"])
def search():
    search_text = request.args.get("search_text")
    user_data = request.args.get("user_data")
    user_data = json.loads(user_data.replace("'", "\""))
    geo_data = request.args.get("geo_data")
    geo_data = json.loads(geo_data.replace("'", "\""))

    return get_search_data(search_text, user_data, geo_data).to_dict()


if __name__ == "__main__":
    shard = int(os.environ["SHARD_ID"])
    data_source = CSV(SEARCH_DOCUMENTS_DATA_FILES[shard - 1])
    app.data = data_source.read_data(to_dict=False)
    app.run(debug=True, host="0.0.0.0", port="5536")
