import os

BASE_DIR = os.sep.join(os.path.normpath(__file__).split(os.sep)[:-3])
BASE_DATA_DIR = os.path.join(BASE_DIR, 'data')
assert os.path.exists(BASE_DATA_DIR)


def datafile(file):
    return os.path.join(BASE_DATA_DIR, file)


USER_DATA_FILE = datafile('users.csv')
GEO_DATA_FILE = datafile('geo.csv')
SEARCH_DOCUMENTS_DATA_FILES = [datafile(f'news_generated.{i}.csv') for i in range(1, 4)]

GEO_ADDR = "geo:5536"
USER_ADDR = "user:5536"
SEARCH_ADDR = "search:5536"
SEARCH_SHARDS_ADDR = "search_in_shards:5536"
DOCS_COLUMNS = ["document", "key", "key_md5"]

SHARD_ADDR_1 = "simple_search_1:5536"
SHARD_ADDR_2 = "simple_search_2:5536"
SHARD_ADDR_3 = "simple_search_3:5536"
